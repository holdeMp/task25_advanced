﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Advanced.Models;

namespace BLL.Services
{
    public class NewsService : INewsService
    {
        IUnitOfWork Database { get; set; }

        public NewsService(IUnitOfWork uow)
        {
            Database = uow;
        }
        public IEnumerable<NewsDTO> GetNews()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<News, NewsDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<News>, List<NewsDTO>>(Database.News.GetAll());
        }
    }
}
