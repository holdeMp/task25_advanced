﻿using DAL1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Advanced.Models;

namespace DAL1.Models
{
    public class ArticleRepository : IArticleRepository<Article>
    {
        private readonly Library1Context _db;
        public ArticleRepository(Library1Context context)
        {
            this._db = context;
        }
        public IEnumerable<Article> GetAll()
        {
            return _db.Articles.ToList();
        }
        public IEnumerable <Tag> GetAllTags()
        {
            return _db.Tags.ToList();
        }
        public string AddTag(int ArticleId,string TagName)
        {
            var article = _db.Articles.FirstOrDefault(i => i.Id == ArticleId);
            if (article.Tags.Any(i=>i.Name==TagName)) 
            {

                return "There is already that tag name on passed article";
            }
            if (_db.Tags.Any(i => i.Name == TagName))
            {
                var tag = _db.Tags.FirstOrDefault(i => i.Name == TagName);
                tag.Articles.Add(article);
                _db.SaveChanges();
                return "Succesfully added article to existing tag";
            }
            ICollection<Article> articles = new List<Article>();
            articles.Add(article);
            article.Tags.Add(new Tag { Name = TagName, Articles = articles });
            _db.SaveChanges();
            return "Succesfully added new tag to article";
        }
    }
}
