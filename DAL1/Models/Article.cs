﻿using DAL1.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Task23_Advanced.Validation;

namespace Task23_Advanced.Models
{
    /// <summary>
    /// Article Model to represent articles.
    /// </summary>
    public class Article
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [ArticleDate(1999)]
        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        [Required]
        public string Content { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public Article()
        {
            Tags = new List<Tag>();
        }
    }
}