﻿using BLL.Infrastructure;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Task23_Advanced.Util;
using Ninject.Web.Mvc;

namespace Task23_Advanced
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            NinjectModule newsModule = new NewsModule();
            NinjectModule reviewsModule = new ReviewsModule();
            NinjectModule articlesModule = new ArticlesModule();
            NinjectModule serviceModule = new ServiceModule("Library1Context");
            var kernel = new StandardKernel(newsModule,reviewsModule, serviceModule,articlesModule);
            System.Web.Mvc.DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}
