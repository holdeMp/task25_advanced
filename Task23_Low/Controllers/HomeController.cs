﻿using AutoMapper;
using BLL.DTO;
using BLL.Services;
using BLL.Interfaces;
using BLL.DTO;
using DAL1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23_Advanced.Models;

namespace Task23_Advanced.Controllers
{
    public class HomeController : Controller
    {
        INewsService newsService;
        public HomeController(INewsService serv)
        {
            newsService = serv;
        }
        // GET: Main
        public ActionResult Index()
        {
            IEnumerable<NewsDTO> newsDtos = newsService.GetNews();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<NewsDTO, NewsViewModel>()).CreateMapper();
            var news = mapper.Map<IEnumerable<NewsDTO>, List<NewsViewModel>>(newsDtos);
            return View(news);
        }
       
    }
}