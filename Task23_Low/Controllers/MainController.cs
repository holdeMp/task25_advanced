﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using PagedList;
using DAL1.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Task23_Advanced.Models;
using System;
using System.Linq;

namespace Task23_Advanced.Controllers
{
    public class MainController : Controller
    {
        private IArticlesService articlesService;
        /// <summary>
        /// Main Contoller to represent Articles
        /// </summary>
        /// <param name="serv"></param>
        public MainController(IArticlesService serv)
        {
            articlesService = serv;
        }
        // GET: Main
        public ActionResult Index(string sortOrder,int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            IEnumerable<ArticleDTO> articlesDtos = articlesService.GetArticles();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDTO, ArticleViewModel>()).CreateMapper();
            var articles = mapper.Map<IEnumerable<ArticleDTO>, List<ArticleViewModel>> (articlesDtos);
            var TagsDtos = articlesService.GetAllTags();
            var TagsMapper = new MapperConfiguration(cfg => cfg.CreateMap<TagsDTO, TagsViewModel>()).CreateMapper();
            var tags = TagsMapper.Map<IEnumerable<TagsDTO>, List<TagsViewModel>>(TagsDtos);
            var dividedArticles = new List<DividedArticlesViewModel>();
            ViewBag.Tags = tags;
            foreach(var article in articles)
            {
                var lessContent = article.Content.Substring(0,199);
                var moreContent = article.Content.Substring(199);
                dividedArticles.Add(new DividedArticlesViewModel
                {
                    Date = article.Date,
                    LessContent = lessContent,
                    MoreContent = moreContent,
                    AuthorName = article.Name,
                    Id = article.Id,
                    Tags = article.Tags
                    
                }) ;
            }
            switch (sortOrder)
            {
                case "name_desc":
                    dividedArticles = dividedArticles.OrderByDescending(s => s.AuthorName).ToList();
                    break;
                case "Date":
                    dividedArticles = dividedArticles.OrderBy(s => s.Date).ToList();
                    break;
                case "date_desc":
                    dividedArticles = dividedArticles.OrderByDescending(s => s.Date).ToList();
                    break;
                default:
                    dividedArticles = dividedArticles.OrderBy(s => s.Id).ToList(); 
                    break;
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(dividedArticles.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult ArticlesByTag(string tagName)
        {
            IEnumerable<ArticleDTO> articlesDtos = articlesService.GetArticles();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDTO, ArticleViewModel>()).CreateMapper();
            var articles = mapper.Map<IEnumerable<ArticleDTO>, List<ArticleViewModel>>(articlesDtos);
            var TagsDtos = articlesService.GetAllTags();
            var TagsMapper = new MapperConfiguration(cfg => cfg.CreateMap<TagsDTO, TagsViewModel>()).CreateMapper();
            var tags = TagsMapper.Map<IEnumerable<TagsDTO>, List<TagsViewModel>>(TagsDtos);
            var dividedArticles = new List<DividedArticlesViewModel>();           
            ViewBag.Tags = tags;
            foreach (var article in articles)
            {
                var lessContent = article.Content.Substring(0, 199);
                var moreContent = article.Content.Substring(199);
                dividedArticles.Add(new DividedArticlesViewModel
                {
                    Date = article.Date,
                    LessContent = lessContent,
                    MoreContent = moreContent,
                    AuthorName = article.Name,
                    Id = article.Id,
                    Tags = article.Tags

                });
            }
            var dividedArticlesByTag = dividedArticles.Where(i=>i.Tags.Any(p=>p.Name==tagName));
            return View(dividedArticlesByTag.ToList());
        }
        [HttpPost]
        public string AddTag(FormCollection form)
        {
            var id = form["Articles"];
            var tagName = form["Tags"];
            var tagNameFromForm = form["TagName"];
            if(tagName == "" && tagNameFromForm.Length < 3)
            {
                return "TagName must be more than 2";
            }
            if (tagName == "")
            {
                var addstatus = articlesService.AddTag(Convert.ToInt32(id), tagNameFromForm);
                if (addstatus == "There is already that tag name on passed article")
                {
                    return "There is already that tag name on passed article";
                }
            }
            else
            {
                var addstatus = articlesService.AddTag(Convert.ToInt32(id), tagName);
                if(addstatus=="There is already that tag name on passed article")
                {
                    return "There is already that tag name on passed article";
                }
            }
            return "Successfully added new tag";        
        }
    }
}