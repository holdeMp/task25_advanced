﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;
namespace Task23_Advanced.Util
{
    public class ReviewsModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IReviewsService>().To<ReviewsService>();
        }
    }
}